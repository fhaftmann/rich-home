#!/usr/bin/python3 -I

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


from pathlib import Path
import os
import pwd
import sys


uid = os.getuid()
user_data = pwd.getpwuid(uid)
user = user_data.pw_name
posix = Path(user_data.pw_dir)


parent = posix.parent

if posix.name != 'posix' or parent.name != user:
    print(f'Home layout of user {user} is not apt for rich directory layout.')
    raise SystemExit(0)


env = parent / 'etc' / 'rich-home.env'

with env.open() as reader:
    for line in reader:
        sys.stdout.write(line)
