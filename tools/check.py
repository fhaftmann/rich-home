#!/usr/bin/python3 -I

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


from typing import NoReturn, Optional
from collections.abc import Iterable, Iterator, Callable
import itertools
import re
from pathlib import Path
import fnmatch
import os
import stat
import subprocess
import argparse
import argcomplete
import sys

import toml
from debian.deb822 import Deb822
from debian.changelog import Changelog
from debian.debian_support import Version


def message(s: str) -> None:

    print(s, file = sys.stderr)


def fail(s: str) -> NoReturn:

    message(s)
    raise SystemExit(1)


def dangling(ss: Iterable[str]) -> str:

    return '\n'.join(itertools.chain([''], ss))


def is_ignored(p: Path) -> bool:

    if not Path('.git').is_dir():
        return False
    result = subprocess.run(['git', 'check-ignore', '-q', '--', p])
    return result.returncode == 0


def read_multifield(s: str) -> tuple[str, str]:

    lines = s.splitlines()
    multi = ['' if line == ' .' else line[1:] for line in lines[1:]]
    return lines[0], '\n'.join(multi)


def pattern_of_copyright(s: str) -> re.Pattern[str]:

    pattern_year = r'\b\d{4}\b'
    before, after = re.split(pattern_year, s, maxsplit = 1)
    return re.compile(re.escape('# Author: ') + re.escape(before) + pattern_year + re.escape(after))


def find_hashbang(hashbang_fragments: Iterable[str]) -> Iterator[Path]:

    re_hashbang = re.compile(r'#!.*\b(?:'
      + '|'.join(re.escape(hashbang_fragment) for hashbang_fragment in hashbang_fragments)
      + r')\b')
    for rel, _, names in os.walk(os.curdir):
        for name in names:
            p = Path(rel) / name
            if p.is_symlink():
                continue
            if p.stat().st_mode & stat.S_IXUSR != stat.S_IXUSR:
                continue
            first_line = p.read_text().splitlines()[0]
            if not re_hashbang.match(first_line):
                continue
            if is_ignored(p):
                continue
            yield p


def find_pys() -> Iterator[Path]:

    for p in Path().glob('**/*.py'):
        if is_ignored(p):
            continue
        yield p


def check_shell_tools() -> None:

    ps = [*find_hashbang(['sh', 'bash'])]
    if ps:
        subprocess.run(['shellcheck', '--check-sourced', '--format=gcc', '--']
          + [str(p) for p in ps], check = True)


def check_python_source() -> None:

    ps = [*find_hashbang(['python', 'python3'])]
    if ps:
        subprocess.run(['flake8', '--show-source', '--config=setup.cfg', '.']
          + [str(p) for p in ps], check = True)
    for p in ps:
        subprocess.run(['mypy', '--strict', '--pretty', '--', str(p)], check = True)


def get_unsnapshotted_version(changelog: Changelog) -> str:

    changelog_version: Optional[Version] = changelog.get_version()
    if changelog_version is None:
        fail('Unable to retrieve version from debian/changelog')
    full_version: Optional[str] = changelog_version.full_version
    if full_version is None:
        fail('Unable to retrieve version from debian/changelog')
    return full_version.split('~')[0]


def check_metadata() -> None:

    version = toml.load('version.toml')['version']

    changelog = Changelog(Path('debian/changelog').read_text(), max_blocks = 1)

    if get_unsnapshotted_version(changelog) != version:
        fail(
            f'Version glitch in debian/changelog, expected »{version}«.\n'
            f'\n'
            f'For a snapshot brand, use\n'
            f'  gbp dch --new-version="{version}" --snapshot\n'
            f'\n'
            f'For a release brand, use\n'
            f'  gbp dch --new-version="{version}" --release\n'
            f'  git add …\n'
            f'  git commit …\n'
            f'  gbp buildpackage --git-tag\n'
        )


def read_licenses() -> Iterator[tuple[Callable[[Path], bool], Callable[[str], bool]]]:

    for block in reversed([*Deb822.iter_paragraphs(Path('debian/copyright').read_text())]):

        if 'Files' not in block:
            continue

        single_name_pattern, more_name_patterns = read_multifield(block['Files'])
        name_patterns = [pattern for pattern
          in ([single_name_pattern] + more_name_patterns.splitlines()) if pattern]
        _, license_header = read_multifield(block['License'])

        header_pattern = re.compile('\n'.join(
          [pattern_of_copyright(block['Copyright']).pattern,
          re.escape('#')]
          + [re.escape(f'# {line}' if line else '#') for line in license_header.splitlines()]))

        def match_header(s: str, header_pattern: re.Pattern[str] = header_pattern) -> bool:
            return bool(header_pattern.search(s))

        for name_pattern in name_patterns:

            def match_name(p: Path, name_pattern: str = name_pattern) -> bool:
                return fnmatch.fnmatchcase(str(p), name_pattern)

            yield (match_name, match_header)


def check_copyright_headers() -> None:

    subprocess.run(['reuse', 'lint'], check = True)

    licenses = [*read_licenses()]
    ps = [*find_pys(), *find_hashbang(['python', 'python3'])]
    for p in ps:
        some_match_header = next((match_header for match_name, match_header in licenses if match_name(p)), None)
        if some_match_header is None:
            fail(f'No copyright matching {p} found in debian/copyright')
        if not some_match_header(p.read_text()):
            fail(f'Bad copyright header in {p}')


parser = argparse.ArgumentParser()
argcomplete.autocomplete(parser)
args = parser.parse_args()

os.chdir(Path(__file__).parent)

check_shell_tools()
check_python_source()
check_metadata()
check_copyright_headers()
